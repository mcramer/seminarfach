# Seminarfach Citations Guide

Bitte dokumentiert folgende Daten in einer .txt Datei (auf Windows im File Explorer einfach Rechtsklick, dann auf Neu, dann Textdatei) an, ich habe jeweils Beispieldatein beigefügt, in denen ihr seht, wie die Datei dann aussehen sollte.

## Literaturquellen (Beispieldatei: literature.txt)

### Obligatorische Angaben (diese Angaben sind immer zu finden und müssen angegeben werden)
* Autor
* Titel
* Erscheinungsjahr
* DOI

### Optionale Angaben (sollten angegeben werden, wenn sie findbar sind)
* Journal (In welchem Magazin ist die Studie veröffentlicht wurden)
* Publisher (hauptsächlich für Bücher oder Studien, welche in Büchern veröffentlicht wurden)
* Seiten
* Band
* Bandnummer
* Erscheinungsmonat

## Onlinequellen (Beispieldatei: online.txt)

### Obligatorische Angaben (diese Angaben sind immer zu finden und müssen angegeben werden)
* Autor (wenn keine Einzelperson oder Gruppe von Personen zu finden ist, reicht der Name der Website oder der Organisation)
* Titel
* URL
* Einsehungsdatum (mit Uhrzeit, die sollte grob stimmen, muss aber nicht auf die Minute perfekt dokumentiert sein)

### Optionale Angaben (sollten angegeben werden, wenn sie findbar sind)
* Erscheinungsmonat
* Erscheinungsjahr

## Buchquellen (Beispieldatei: book.txt)

## Obligatorische Angaben (diese Angaben sind immer zu finden und müssen angegeben werden)
* Autor
* Titel
* Erscheinungsjahr
* ISBN
* Publisher

## Optionale Angaben (sollten angegeben werden, wenn sie findbar sind)
* Erscheinungsmonat
* DOI (meistens nicht angegeben, nicht nötig wenn eine ISBN existiert)

## Vielen Dank!

