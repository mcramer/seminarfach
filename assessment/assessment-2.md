## Selbst- und Gruppeneinschätzung Seminarfach

### Schüler: Marius Cramer

### Gruppenmitglieder:

* Janice Greven
* Luise Reuchsel

</br>

Nun im Nachhinein bin ich durchaus zufrieden mit unserer Seminarfacharbeit. Außerdem bin
ich froh, dass wir uns genug Zeit eingeteilt haben und auch früh genug mit der Arbeit
begonnen haben. Aus diesem Grund hatten wir nie wirklich großen Zeitdruck während der
Erstellung, nur am Ende sind die Deadlines ein paar Tage verrutscht, weil wir aber eine
Woche als Pufferzeit eingeplant hatten war das kein Problem für uns. Ich persönlich habe
mir während der Ausarbeitungsphase der Arbeit einiges an Fach- und Sachkenntnissen
angeeignet und mich tiefgründig vorbereitet. Wir haben uns für den ganzen
Erstellungsprozess jeweils Deadlines für gestaffelte Zwischenzielsetzungen gestellt und
deshalb lagen wir praktisch immer in der Zeit. Ich habe mich bewusst und zielgerichtet in
den Prozess eingebracht, habe die Leitung übernommen, aber auch immer die anderen
Gruppenmitglieder eingebunden und ihre Meinungen erfragt. Immer wenn es
Meinungsunterschiede gab haben wir abgestimmt und Konflikte so beglichen. Ich habe die
formelle Erstellung der Arbeit übernommen, indem ich mit der LaTeX-Schriftsatzsprache
die Arbeit erstellt habe. Außerdem hab ich die Erstellung der Bibliographie, der
Sortierung und Auflistung der Quellen sowie die Erstellung der Fußnoten übernommen. Ich
habe meiner Meinung nach durhweg Motivation und Engagement gezeigt, da das Thema mir auch
persönlich auch am Herz liegt. Zu manchen Zeitpunkten, wenn zum Beispiel durch Missachtung
von Deadlines ein Zeitverzug entstand, habe ich versucht die Arbeit am laufen zu halten
und den Zeitplan einzuhalten. Da sich keines der Gruppenmitglieder je mit Kritik an mir
geäußert hat gehe ich davon aus, dass beide mit meinem Druck in solchen Situationen
einverstanden waren, oder ihn zumindest akzeptiert haben. Im Nachhinein sehe ich dies als
eigenen Fehler ein, indass ich nicht dafür gesorgt habe den Erstellungsprozess noch mehr
zu staffeln und die Gruppe dazu zu drängen früher anzufangen. Immer wenn wir auf Probleme
gestoßen sind haben wir diese als Gruppe besprochen und dann abgestimmt um einen
Kompromiss als Lösung zu finden, so haben wir alle Konflikte schenll und effektiv gelöst.
Ich habe immer versucht meine Argumente und Meinungen sachlich zu untermauern und so
gesunde Diskussionen zu führen. Die Recherchearbeit lief im Großen und Ganzen gut und wir
haben uns durch unser anfängliche Zeitplanung auch ausreichend viel Zeit dafür
geschaffen. Mir persönlich fiel es nicht schwierig Informationsmaterial zu beschaffen, da
ich eine sehr gut ausgebaute Grundlage in der englischen Sprache besitze, meine
Gruppenmitglieder hatten damit mehr Probleme, haben diese aber imemr gut gelöst. Alles in
Allem haben wir weder Einzel- noch Gruppenarbeit praktiziert, vielmehr haben wir uns
regelmäßig abgesprochen und auch getroffen um den Stand der Arbeit zu besprechen und zu
begleichen, haben unsere eigenen Arbeitsanteile jedoch jeweils alleine erarbeitet. So
konnten wir, meiner Meinung nach, sehr effektiv und zeitsparend arbeiten. Wir haben
durchweg gestaffelt gearbeitet, also immer Teilschritte erstellt und diese als
Zwischenziele angesehen und nach und nach abgearbeitet. Wir haben nun auch schon
angefangen erste Überlegungen für die Vorstellung der Ergebnisse, also unser Kolloquium,
aufzustellen. Alles in Allem bin ich sehr zufrieden sowohl mit meinen Gruppenmitgliedern
als auch mit unserer Arbeit selbst. Wir hätten mit Sicherheit zwar noch effektiver
arbeiten können und somit vielleicht auch eine im Allgemeinen bessere Arbeit abgeben
können, aber auch die Umstände der Seminarfacharbeit müssen beachtet weren und in
Angesicht dieses Hintergrunds bin ich doch sehr zufrieden.
