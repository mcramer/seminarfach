# Strukturübersicht Seminarfacharbeit

## Autoren
* Janice Greven
* Luise Reuchsel
* Marius Cramer

## Arbeitstitel: Überleben auf dem Mars — Strategien zur Erzeugung von Wasser, Sauerstoff und Nahrungsmitteln als Lebensgrundlage

## Aufbau
* Titelblatt
* (Vorwort)
* Inhaltsverzeichnis
* Einleitung
	* Ziel und Motivation der Arbeit
	* bisherige Forschungen/Experimente
	* Wichtigkeit der Arbeit erläutern
* Abhandlung zum Thema
	* 1 Bedingungen
		* 1.1 Eigenschaften von Oberflächenmaterialien (Janice, Luise)
			* Beschaffenheit und Struktur des Bodens
			* Chemische Zusammensetzung des Bodens
				* Regolith
				* Staub
				* Mineralien
		* 1.2 Physikalische Eigenschaften
			* Gravitation (Marius)
			* Magnetfeld (Marius)
			* Wärmeverhältnisse (Janice, Luise)
   		* 1.3 Charakteristika der heutigen Marsatmosphäre (Marius)
			* Atmosphärische Struktur
			* Wolken und Staubstürme
			* Klima
		* 1.4 Wasservorkommen
			* Ursprung des Wassers
			* Wasserquellen
	* 2 Erzeugung von Trinkwasser (Marius)
		* 2.1 Wasserpurifikationssysteme (Water Recovery System -- WRS)
			* Urinverarbeitungseinheit (Urine Processor Assembly -- UPA)
			* Wasserverarbeitungssysteme (Water Processor Assembly -- WPA)
			* Experiment
		* 2.2 In-situ-Ressourcennutzung (ISRU)
			* H20 Extraktions aus Boden
				* mit Wärme (Nebenprodukt der Entsalzung)
			* RESOLVE rover
			* Chemische Reaktionen
				* Sabatier Reaktion
	* 3 Erzeugung von Sauerstoff (Marius)
		* 3.1 Luftrevitalisierungssysteme (Air revitalization system -- ARS)
			* Kohlenstoffdioxidentfernung (Carbon Dioxide Removal -- CDR)
			* Spurenverunreinigungskontrollsysteme (Trace Contaminant Control -- TCCS)
			* Advanced Closed Loop System (ACLS)
			* Hauptbestandteilanalysator (Major Constituent Analyser -- MCA)
		* 3.2 Sauerstofferzeugung (Oxygen Generation System -- OGS)
			* Elektrolytischer Sauerstoffgenerator (Electrolytic Oxygen Generator)
			* Mars Oxygenator
   			* Festbrennstoff-Sauerstofferzeugung (Solid Fuel Oxygen Generation -- SFOG)
			* MOXIE Rover
	* 4 Erzeugung von Nahrung
		* 4.1 Anpflanzen von Nahrungsmitteln (Luise)
			* geeignete Umgebung und Technologien für den Anbau
			* geeignete pflanzliche Lebensmittel
			* einhergehende Probleme und Gefahren
			* Selbstexperiment im Vergleich zu der Studie der Universität Wageningen
			* Erschaffen eines landwirtschaftlich-nachhaltigen Ökosystems
		* 4.2 Alternative Strategien (Janice)
			* Herstellung Nahrung aus eigenen Abfällen
			* aus Bakterienkulturen am Beispiel der Chlorella vulgaris
* Schlussbetrachtungen
	* Ausblick
	* Abstaktion -- Interpretation der Ergebnisse
	* Wichtigkeit/Bedeutung der Ergebnisse aufzeigen
	* Wurde das Ziel der Einleitung/Arbeit erreicht?
* Literatur- und Quellenverzeichnis
* Anhang
* eidesstaatliche Erklärung
