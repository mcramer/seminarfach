# Einführung
Der Mars ist seit Hunderten von Jahren von großer Begeisterung für Menschen. Die rote
Färbung gab dem Planeten seinen Namen, nach dem römischen Kriegsgott Mars. Im Jahr 1609
konnte Galileo Galilei noch nicht mehr als eine rote Scheibe mit seinem Teleskop erkennen.
50 Jahre später wurden zum ersten Mal Albedomarkierungen von Christiaan Huygens
dokumentiert, dies führte zur Berechnung der Rotationsdauer des Planeten, damals noch als
rund 24h berechnet. 1666 wurden dann zum ersten Mal die hellen Polarkappen von Giovanni
Cassini entdeckt, sein Neffe fand später heraus, dass die südliche Polarkappe nicht
deckungsgleich mit der südlichen Rotationskappe ist und dass rund um die Polarkappen
dunkle Bänder erscheinen, dies interpretierte er schon damals als Schmelzwasser. Sir
William Herschel beobachtete den Mars von 1777 bis 1783 und bestimmte, das Mars'
Rotationsachse um 30\textdegree{}, dies führte zur Erkenntnis, das Mars vier Jahreszeiten
durchlebt; außerdem wurde die Rotationsperiode genauer bestimmt, auf 24 Stunden, 39
Minuten und 21,67 Sekunden. Während der Nahannährung von 1830 wurde die erste komplette
Karte des Mars angefertigt und 1840 von Johan von Mädler und Wilhelm Beer veröffentlicht, 
gleichzeitig redifinierten sie Mars' Rotationsperiode als 24 Stunden, 37 Minuten und
22,6 Sekunden, der heute anerkannte Wert ist nur um 0,1 Sekunden anders. 
Das Ziel unserer Arbeit ist, Methoden zur Herstellung von Nahrung, Wasser und Atemluft zu
untersuchen, als Grundlage werden davor die Bedingungen auf dem Mars geklärt. Außerdem
unterlegen wir unsere Ausarbeitungen und Ergebnisse mit zwei Experimenten, eines zum Anbau
von Nahrung und eines zur Aufarbeitung von Wasser. Unsere Motivation ist, die Wichtigkeit
dieses Themas für die Vergangenheit, Gegenwart und Zukunft aufzuzeigen und gleichzeitig
Interesse am Gebiet der Raumfahrt und Planetenwissenschaft zu wecken, vorallem in den
jungen Generationen, für welche dieses Thema eine immer größere Rolle spielen wird.

# Ausarbeitung

## 1.2 Physikalische Eigenschaften

In diesem Teil werden drei der wichtigsten physikalischen Eigenschaften erläutert: die
Gravitation, das Magnetfeld sowie die Wärmeverhältnisse auf und unter der Oberfläche.

### Gravitation [Marius]

Eine wichtige Veränderung auf das Leben von Menschen auf dem Mars ist die
niedrige Gravitation. Während auf der Erde eine Fallbeschleunigung von $~9.806ms^{-2}$
herrscht, werden Objekte auf Mars' Oberfläche mit nur $~3.117ms^2$. Die Fallbeschleunigung
lässt sich mit der Gleichung

	$$g = G \times M \times R^{-2}$$

berechnen, wobei $G$ die universelle Gravitationskonstante ist, welche 1798 von Henry
Cavendish experimentell entdeckt wurde ($G = 6.67 \times 10^{-11} m^3 \times s^{-2} \times kg^{-1}$),
$M$ die Masse des Objektes (in unserem Fall die Masse des Planeten Mars; $M = 6.4185 \times 10^{23} kg)
und $R$ die Distanz vom Massemittelpunkt (für unsere Berechnung entspricht $R$ dem mittleren
Äquatorialradius; $R = 3396200 m$). Setzt man die Werte nun ein entsteht
folgende Gleichung:

    $$g = 6.67 \times 10^{-11} \times 6.4185 \times 10^{23} \times 34396200^{-2} = 3.117ms^{-2}$$

Wird diese nun berechnet, so erhält g den Wert $3.117ms^{-2}$. Dies entspricht ungefähr 38%
der Fallbeschleunigung, die auf der Erde auf Normal Null herrscht. Ein Mensch würde sich
also rund 62% leichter fühlen als auf der Erde. Die mindere Fallbeschleunigung, und damit
die mindere Anziehungskraft von 0.378g, sind ein Faktor, welcher beim Anbau von Pflanzen
und wichtigen Tätigkeiten auf einer Marsstation nicht vergessen werden dürfen. So wachsen
Pflanzen zwar höher, die Sprossachsen und Stängel werden aber deutlich schmaler und
schwächer, da sie weniger stark gegen die Anziehungskraft arbeiten müssen. Dies bedeutet
allerdings, dass Pflanzen, welche auf Mars angebaut werden, aufgrund ihrer geringeren
Größe weniger Nährstoffe bieten.

### Magnetfeld [Marius]

Da Mars' Kern mittlerweile hauptsächlich fest ist und daher nur einen leichten Dynamoeffekt
aufweist (die Bewegung flüssiger Metalle im Kern der Erde erzeugt das Erdmagnetfeld)
besitzt Mars nur noch ein sehr geringes Magnetfeld. Es wird angenommen, dass Mars sein
Magnetfeld vor rund vier Milliarden Jahren verlor. Jedoch hält der Regolith teilweise noch
die Überreste, welche dann lokale Magnetfelder und somit Magnetosphären erzeugen, diese
reichen aber nicht aus, um den Planeten vor kosmischer und elektromagnetischer Strahlung
zu schützen. Daher haben diese lokalen Magnetfelder keine Relevanz für den Menschen im
Bezug auf die Kolonialisierung.

### Wärmeverhältnisse [Janice]


## 1.3 Charakteristika der Marsatmosphäre

Mars' Atmosphäre zerfällt seit langer Zeit und ist menschlichem Leben gegenüber sehr feindlich,
angefangen mit gefährlicher kosmischer Strahlung bis hin zu extremer Kälte und monatelangen
Staubstürmen.

### Atmosphärische Struktur [Marius]

Mars' Atmosphäre besteht zu sehr großen Teilen aus Kohlenstoffdioxid, genauer gesagt zu
95.32%, weitere Hauptbestandteile sind Stickstoff (2.70%), Argon (1.60%), Sauerstoff
(0.13%) und Kohlenstoffmonoxid (0.08%), dies erzeugt eine für menschliches Leben
gefährliche Atmosphäre. Weiterhin herrscht auf Bodenhöhe ein atmosphärischer Druck von
durchschnittlich 0.0059atm, also rund 170 Mal weniger Druck als auf der Erde auf Seehöhe.
Dieser sehr geringe Druck führt dazu, dass Menschen ohne Druckanzüge nicht auf Mars'
Oberfläche überleben könnten. Weiterhin benötigen sie ein Atemluftsystem, welches sie mit
gewohnter Luft (78% Stickstoff, 21% Sauerstoff, andere Gase) versorgt und vor der sehr
Kohlenstoffdioxid-reichen Atmosphäre schützt. Mittlerweile ist sich die Forschungsgemeinschaft
einig, dass Mars in seiner frühen Geschichte eine weitaus dichtere und wärmere
Atmosphäre besaß, manche Forscher denken sogar sie könnte dichter als die heutige
Erdatmosphäre gewesen sein, dies ist aber noch umstritten. Akzeptiert ist aber, dass die
Temperaturen damals so hoch gewesen sind, dass Wasser die Möglichkeit hatte in flüssiger
Form zu existieren und Kanäle und Flüsse zu formen, so wie sie heute auf Mars sichtbar
sind.\autocite{bib17} Mars' Atmosphäre wird seit dem Verlust der Magnetosphäre dauerhaft
vom Sonnenwind und kosmischer Strahlung angegriffen, was dazu führt, dass Atome der
Atmosphäre konstant herausgerissen werden.

### Staubstürme und Wolken [Janice]

Staubstürme sind eine Normalität auf Mars, sie treten mehrere Male im Jahr auf und können
von wenigen Wochen bis zu einigen Monaten anhalten. Besonders oft treten Sonnenstürme am
Perihel auf, also wenn der Mars der Sonne am nächsten steht, da der Planet 40% mehr
Sonnenlicht erhält als zum Aphel, dem sonnenfernsten Punkt in seiner Umlaufbahn. Diese
Zunahme an Sonnenlicht führt zu einer Temperaturänderung von bis zu +20°C zum Perihel, die
höhere Temperatur ist der Grund für Staubstürme kontinentaler Größe auf der Oberfläche.
Die wiederum stoßen wie bei den Wolken Staubpartikel auf Höhen von bis zu 80km, welche
dann vom Sonnenlicht erhitzt werden und die gesamte Atmospähre wieder um 15-25°C erwärmen.
Am Aphel jedoch erzeugt das kalte Klima planetweit Wassereiswolken auf einer Höhe von bis
zu 10km. Staub von kleinen Staubstürmen zu dieser Zeit kann also nicht höher als diese
Wolken steigen, außerdem wirken die feinen Staubpartikel als Kondensationskerne, es bilden
sich also Wolken um sie herum. So kommt nun keine Sonne mehr an den Staub und er friert
und fällt zum Boden. Schon mehrmals konnten planetweite Staubstürme beobachtet werden,
Forschungen zeigten, dass die Chance für einen planetweiten Staubsturm bei eins zu drei
liegt und somit relativ häufig solch riesige Stürme auftreten. Auch während des Perihels
bilden sich um die von Stürmen aufgewirbelten Staubpartikel Wolken, Mars Express fand,
dass diese auf bis zu Höhen von 100km existieren. Im Gegensatz zu den Wolken der Erde
bestehen sie jedoch nicht aus Wasserdampf, sondern hauptsächlich aus Kohlenstoffdioxid,
der Atmosphäre geschuldet. Von der marsianischen Oberfläche können sie wegen ihrer großen
Höhe und geringen Sichtbarkeit nur nach Nachteinbruch gesehen werden, da sie dann durch
das restliche Sonnenlicht gegenüber dem dunklen Nachthimmel leuchten. Somit sind sie
vergleichbar mit den leuchtenden Nachtwolken auf der Erde.

### Klima [Janice]

Da der Mars eine vergleichsweise sehr dünne Atmosphäre besitzt, speichert diese
Temperaturen sehr schlecht. Das bedeutet, dass die Temperaturdifferenz von Tag zu
Nachtzeiten auf Mars im Gegensatz zur Erde sehr hoch sind. In Deutschland ist im Juli die
durchschnittliche Maximaltemperatur (tagsüber) ungefähr 10°C während die Minimaltemperatur
(nachts) rund 22°C beträgt, dies ergibt eine tägliche Temperaturdifferenz von 12°C. Auf
Mars (Gale Krater) ist die Maximaltemperatur im Juli am Äquator mittags bis zu 20°C und
die Minimaltemperatur beläuft sich auf bis zu -84°C. Hier ist die tägliche Temperaturdifferenz
also 104°C, was ein Symptom der dünnen Atmosphäre ist, Wärme kann hier nicht so gut
wie in der erdischen Atmosphäre gespeichert werden. An den Polen kann die Oberflächentemperatur
bis auf -153°C sinken. Spirit Rover zeichnete eine Rekord-LUfttemperatur von 35°C
im Schatten auf und maß regelmäßig Temperaturen über 0°C.\autocite{bib13} Laut NASA
beträgt die durchschnittliche Oberflächen Temperatur auf Mars rund -63°C.\autocite{bib12}
Interessant ist, dass während die jährlichen Wintertemperaturen sich immer auf eine
Differenz von ±1°C beliefen, die jährlichen Sommertemperaturen sich durch bis zu ±6°C
unterschieden. Dieser Unterschied ist unerwartet und bisher noch nicht verstanden. Mars
Orbiter Camera, ein Kamerasystem auf der Mars Global Surveyor Sonde, zeigte über eine
Zeitspanne von 30 Monaten, dass das marsianische Wetter weitaus stabiler und voraussagbarer
als das erdische ist. Dies kommt wahrscheinlich davon, dass Mars keine oberflächlichen
Ozeane besitzt, welche auf der Erde die Quelle für viele jährliche Wettervariationen sind.

## 1.4 Wasservorkommen

Das Wasser auf Mars existiert, das weiß man nun seit einiger Zeit. Dank moderner
Radartechnik ist es seit den letzten paar Jahren sogar möglich unter die Oberfläche zu
schauen. Mit Hilfe hochentwickelter Technologien können Forscher heutzutage abschätzen wo
und wie viel Wasser wohl einmal auf dem Mars vorhanden war. So sind zum Beispiel große
Flussdeltas, Schluchten, Täler und Seegründe auf Mars' Oberfläche zu sehen. Noch immer
wird die Landschaft auf dem Mars durch flüssiges Wasser verändert, zum Beispiel durch
Erosionsrinnen an und in Kratern, welche auf Grund von kleinen Solequellen in den wärmsten
Monaten entstehen. Doch wo kommt das Wasser auf dem Mars her?

### Ursprung des Wassers [Marius]

Mittlerweile weiß man, dass ein Großteil des marsianischen Wassers wohl von Meteroiten,
Kometen und Planetoiden (auch Asteroide genannt) stammt. Beim Aufschlag dieser Kleinkörper
findet Ausgasung statt, es tritt also Gas, zum Beispiel Wasserdampf, aus festem oder
flüssigem Material innerhalb dieser Körper aus. Es wird geschätzt, dass ein großer Anteil
des marsianischen Wassers so auf den Mars kam.

### Wasserquellen [Marius]

Heutzutage befindet sich Wasser hauptsächlich in der Atmosphäre und im Untergrund als Eis
oder seltener als Sole. Im Juli 2018 gab die Europäische Raumfahrt Agentur (ESA) bekannt,
dass sie mit Hilfe ihres Mars Express Orbiters einen Untergrundsee aus flüssigem Wasser
entdeckt haben. Dieser liegt 1-2km unter der Oberfläche und hat eine geschätzte Wassertemperatur
von -10°C bis zu -30°C, damit das Wasser hier flüssig bleibt muss es einen sehr
hohen Salzgehalt haben. Das auf Mars immernoch flüssiges Wasser existiert, wenn auch weit
untergrund, ist ein gutes Zeichen für potentielles mikrobielles Leben, da Wasser eine der
wichtigsten Zutaten dafür ist.
In einem Großteil der Meteroiten, welche von Mars kommen und auf der Erde eingeschlagen
sind, ist Wasser als Eis enthalten. Viele der über 60 Meteroiten, welche von Mars kamen,
scheinen vor ihrem Auswurf in das Weltall Kontakt mit flüssigem Wasser gehabt zu haben.
Eine der Marsmeteroitarten, Nakhliten, sind wahrscheinlich vor rund 620 Millionen Jahren
in Kontakt mit flüssigem Wasser gewesen und dann vor 10.75 Millionen Jahren durch einen
Kleinkörpereinschlag auf der Oberfläche in das Weltall geschleudert wurden, innerhalb der
letzten 10.000 Jahre sind sie dann auf der Erde gelandet.

# 2 Gewinnung von Trinkwasser

Für die Anschaffung von trinkbarem Wasser auf Mars gibt es zwei vorherrschende Konzepte,
zum einem das Closed-Loop-System, in welchem ein anfängliches Volumen an Trinkwasser so
effizient wie möglich gereinigt und wiederverwendet wird, bis die Bestände wieder aufge-
füllt werden müssen. Zum anderen gibt es das Open-Loop-System, in dem Ressourcen aufge-
Braucht werden ohne sie je wieder aufzubereiten; die Ressourcen für beide Systeme können
mitgebracht oder lokal gefördert sein. Das praktischste Konzept für die Erzeugung von
frischem Trinkwasser ist wahrscheinlich aber eine Mischung beider Systeme und beider
Wasserquellen.

## 2.1 Wasserpurifikationssysteme (Closed-Loop)

Ein Wasserpurifikationssystem (WRS) ist ein wichtiger Teil eines Lebenserhaltungssystems,
zuständig für die Aufbereitung von Abwasser und atmosphärischem Wasserdampf zu Trinkwasser.
Nicht nur auf der Internationalen Raumstation ist so ein WRS von großer Bedeutung, sondern
auch in extraterrestrischen Umgebungen, denn sauberes, trinkbares Wasser ist dort wegen der
schwierigen Transport- und Erzeugungsbedingungen nur durch Wiederverwendung erhältlich.
Ein Wasserpurifikationssystem besteht aus zwei grundlegenden Modulen, der Urin- und der
Wasserverarbeitungseinheit.

### Urinverarbeitungseinheit (UPA) %TODO [Synonyme - Bestandteile]

In diesem Teil des WRS wird ein Prozess namens Vakuumdestillation sowie eine Zentrifuge
genutzt, um die Bestandteile der Flüssigkeiten und Gase zu trennen. Bei einer Vakuum-
destillation wird eine Destillation unter Vakuum durchgeführt, um die Siedepunkte der
einzelnen Bestandteile zu senken und somit den Vorgang chemisch und energetisch zu
vereinfachen. Die Zentrifuge wird gebraucht, um die Schwerkraft der Erde zu simulieren,
damit eine Trennung der Bestandteile mittels der unterschiedlichen Stoffdichten möglich ist.
Bei der UPA der Internationalen Raumstation wurde ursprünglich eine Wasserwiederverwendungsrate
von rund 85% angestrebt, jedoch führten die erhöhten Calciumwerte im Urin, welche durch die
Abnahme der Knochendichte entstanden, dazu, dass in den Verarbeitungssystem ein
Calciumsulfatniederschlag stattfand, welcher die Wiederverwendungsrate auf 70% senkte.

### Wasserverarbeitungseinheit (WPA) %TODO [Bildanhang updaten]

Dieses Modul ist dafür zuständig, das Wasser aus der UPA sowie anderen Abwasserquellen zu
vereinen und dann Gasbestandteile sowie Feststoffe herauszufiltern bevor das Wasser durch
Filterbetten und einen katalytische Hochtemperaturreaktor läuft. Danach wird das verbliebene
Wasser von Sensoren geprüft und gegebenenfalls noch einmal durch die WPA gereinigt bis es
den vorgegebenen Reinheitsstandards entspricht. Die für eine Raumfahrtmission verwendeten
Filterbetten können mit einfachsten Mitteln veranschaulicht werden. So kann ein solcher
Wasserfilter aus verschieden feinen Regolithschichten, zum Beispiel Kiesel- und Sandschichten,
sowie Watte und Aktivkohle, innerhalb einer großen Wasserflasche gebaut werden. Im Bildanhang
[x] sind dazu Bilder zu finden.

## 2.2 In-Situ-Ressourcennutzung (ISRU)

Wie schon erwähnt, können nicht nur mitgebrachte Wasserressourcen genutzt werden, denn
Mars beherbergt enorme Mengen an Grundwasser, in fester und flüssiger Form; die
Atmosphäre enthält weiterhin Wasserdampf, welcher eingefangen werden könnte, um ihn zu
Trinkwasser aufzuarbeiten. Im Jahr 2018 veröffentlichten R. Orosei et al. eine Studie,
in welcher sie bekanntgaben, dass mit Hilfe des Mars Express Marsorbiters der ESA einen
subglazialen, also einen sich unterhalb eines Gletschers befindlichen, Wassersee fanden.
Dies erreichten sie mit Hilfe des eingebauten MARSIS-Instruments, ein Radarsystem für die
Sondierung des marsianischen Untergrunds sowie der Ionosphäre. Dabei durchdringen die Radar-
emissionen den Boden und werden je nach Wellenlänge vom Untergrund reflektiert. Anhand
der Reflektivität des reflektierenden Stoffes kann die Zusammensetzung jenes Materials
relativ genau bestimmt werden. Außerdem wird die Tiefe des Materials durch die Reflektions-
zeit der Radaremissionen bestimmt, umso länger die Strahlen zurück zum Sensor des MARSIS
benötigen, desto tiefer liegen sie im Untergrund. Damit das Wasser, welches in diesem sub-
glazialen See enthalten ist, bei den extrem niedrigen Temperaturen der südpolaren
Schichtablagerungen, also Mars' südliche Eisdecke, von -113°C an der Oberfläche bis -3°C
am Boden der Ablagerungen noch noch in flüssiger Form bleibt, muss es einen einen extrem
hohen Salzgehalt haben. Solch eine Wasser ist für Menschen nicht mehr sicher, der mensch-
liche Körper kommt mit solchen Mengen an Salz nicht mehr klar. Mit einem Prozess namens
Entsalzung könnte dieses Wasser jedoch zu Trinkwasser verarbeitet werden. Es gibt ver-
schiedene Wege eine Entsalzung durchzuführen, jedoch ergibt eine Solardestillation am
meisten Sinn, da sie technisch simpel und großflächig durchführbar ist. Hierbei wird Salz-
wasser durch Sonnenenergie zum Verdampfen gebracht, wonach der Wasserdamp dann an einer kalten
Oberfläche kondensiert und wieder aufgefangen wird. Solch eine Anlage kann mit sehr großen
Verarbeitungsvolumen entwickelt werden, und der Destillationsprozess verläuft praktisch ohne
äußere Energiefzufuhr. So ist dieser Vorgang sehr energiesparend und kann große Mengen
Frischwasser produzieren, ist jedoch zu 100% von der Sonnenenergie und damit vom Wetter
abhängig, auf dem Mars ist dies noch ein größeres Problem als auf der Erde, da wie im
Abschnitt ### Klima und ### Wolken und Staubstürme beschrieben wurde, Staubstürme fast
jegliche Sonnenenergie von der Oberfläche abschneiden. Deshalb benötig man als Notfall-
sicherung weiterhin ein Vakuumdestillationssystem, welches jedoch durch externe Energie,
zum Beispiel Kernenergie, betrieben werden muss, und dazu viel kleinere Mengen pro Zeit-
einheit verarbeiten kann.
Neben der Entsalzung gibt es noch eine andere bewährte Methode zur Frischwassergewinnung:
Die Sabatier-Reaktion. Bei diesem chemischem Vorgang reagiert Wasserstoff mit Kohlenstoff-
dioxid unter Temperaturen von 300°C bis 400°C, erhöhtem Druck und mit Hilfe eines Nickel-
Katalysators. Bei dieser Reaktion entstehen Methan, ein vielversprechender Raketentreibstoff,
sowie Wasser.

$$CO2 + 4H2 ->[400°C] CH4 + 2H2O$$

Da Mars Atmosphäre zu über 96% aus Kohlenstoffdioxid besteht, stellt die An-
schaffung jenes Stoffe kein Problem dar. Auch den Wasserstoff erhält man als Nebenprodukt
einer anderen Reaktion, der Elektrolyse, welche in einem Lebenserhaltungssystem zur
Sauerstoffproduktion genutzt werden kann. Damit wird die Sabatier-Reaktion zu einem wichtigen
Teil innerhalb des Closed-Loop-Systems.

### RESOLVE

Die US-amerikanische Raumfahrtbehörde NASA arbeitet an einem Projekt namens RPM, was für
Resource Prospector Mission, also Ressourcenprospektmission, steht. Das Ziel dieser Mission
ist, die In-Situ-Ressourcennutzung (ISRU) auf dem Mond zu testen, um abzuschätzen, wie er-
folgreich solch ein Konzept auf dem Mars sein könnte. Im Zentrum der RPM steht RESOLVE,
ein Werkzeug zur Ausgrabung von Mondregolith, die NASA erhofft sich in ihren Proben, welche
an den Polen des Mondes entstehen sollen, Sauerstoff zu finden, denn dieser macht 42% der
Masse des Mondregoliths aus. Solch ein Fund würde der Treibstoffproduktion auf dem Mond die
Türen öffnen und somit die Raumfahrt in die Zukunft katapultieren. Außerdem ist ein solches
Projekt wichtiges Lehrmaterial auf dem Werg zum Mars, auf welchem ähnliche Technologien
für Wassereis genutzt werden könnten.
